package com.thanksys.brewery.microservice.application;

import com.thanksys.brewery.microservice.api.exception.NotFoundException;
import com.thanksys.brewery.microservice.dto.BeerDTO;
import java.util.Objects;
import org.springframework.stereotype.Component;

@Component
public class FindBeersByCountry {


    public BeerDTO[] call(String country) {
        if (Objects.equals(country, "belgian")) {
            return new BeerDTO[]{
                    leffe(), duvel(), stella()
            };
        } else {
            throw new NotFoundException(country + " is not a real country");
        }
    }

    private BeerDTO stella() {
        return BeerDTO.builder()
                .name("stella")
                .alcoholPercentage(5)
                .build();
    }

    private BeerDTO duvel() {
        return BeerDTO.builder()
                .name("duvel")
                .alcoholPercentage(8)
                .build();
    }

    private BeerDTO leffe() {
        return BeerDTO.builder()
                .name("leffe")
                .alcoholPercentage(6)
                .build();
    }
}
