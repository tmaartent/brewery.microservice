package com.thanksys.brewery.microservice.api;

import com.thanksys.brewery.microservice.application.FindBeersByCountry;
import com.thanksys.brewery.microservice.dto.BeerDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BeerController {
    private final FindBeersByCountry findBeersByCountry;

    public BeerController(FindBeersByCountry findBeersByCountry) {
        this.findBeersByCountry = findBeersByCountry;
    }

    @GetMapping("/beers/{country}")
    private ResponseEntity<BeerDTO[]> getCountryBeers(@PathVariable("country") String country) {
        return new ResponseEntity<>(findBeersByCountry.call(country), HttpStatus.OK);
    }
}
