import org.springframework.cloud.contract.spec.Contract

Contract.make {
    description("When a GET request to /beers/belgian is done, then a list of all our beers should be returned")
    request {
        method "GET"
        url "/beers/belgian"
    }
    response {
        status 200
        body([
                [
                        name            : "leffe",
                        alcoholPercentage: 6
                ],
                [
                        name            : "duvel",
                        alcoholPercentage: 8
                ],
                [
                        name            : "stella",
                        alcoholPercentage: 5
                ]
        ])
        headers {
            contentType(applicationJson())
        }
    }
}