import org.springframework.cloud.contract.spec.Contract

Contract.make {
    description("When a GET request to /beers/narnia is done, then a 404 error code is returned")
    request {
        method "GET"
        url "/beers/narnia"
    }
    response {
        status 404
    }
}