package com.thanksys.brewery.microservice;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.standaloneSetup;

import com.thanksys.brewery.microservice.api.BeerController;
import com.thanksys.brewery.microservice.application.FindBeersByCountry;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = {BeerController.class, FindBeersByCountry.class})
@RunWith(SpringRunner.class)
public abstract class ContractBaseClass {
    @Autowired
    private BeerController beerController;

    @Before
    public void setup() throws Exception {
        standaloneSetup(beerController);
    }
}
